from django.db import models

# Create Note model that contains to, from, title, and message.

class Note(models.Model):
    untuk = models.CharField(max_length=100)
    dari = models.CharField(max_length=100)
    judul = models.CharField(max_length=500)
    pesan = models.TextField()
