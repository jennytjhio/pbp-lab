Pertanyaan 1
Apakah perbedaan antara JSON dan XML?

Jawab:
XML adalah singkatan dari “Extensive Markup Language", seperti namanya XML merupakan sebuah bahasa mark up. XML digunakan untuk menyimpan dan menukar data, tetapi tidak untuk menampilkan data yang ada. XML sendiri merupakan turunan dari Standard Generalized Markup Language (SGML). Hal tersebut membuat code XML mudah dipahami oleh manusia dan mesin/komputer.

Sedangkan, JSON yang memiliki kepanjangan JavaScript Object Notation, adalah sebuah format penyimpanan dan pertukaran data agar data dapat diuraikan dengan baik. Seperti namanya, JSON merupakan turunan dari JavaScript, akan tetapi JSON tetap dapat digunakan di berbagai bahasa pemrograman lain seperti Python, C, C++, C#, Java, dll. JSON memiliki format berbentuk mapping yang memiliki key serta value.

Untuk penggunaannya, XML digunakan apabila project yang sedang kita buat memerlukan proteksi keamanan data. Sedangkan, apabila data yang ada pada projek kita tergolong sederhana, maka kita sebaiknya memilih JSON. Hal ini dikarenakan bentuk penyimpanan JSON yang berupa mapping lebih sederhana sehingga pemrosesan datanya pun akan lebih cepat.

Berikut adalah gambar yang memuat tabel perbedaan XML dan JSON:
https://lh3.googleusercontent.com/qMDPG69YVdLE162e6s7UWwtfvXZca9AweEj2dpzLWhk-hdt9cIperBO7DO1fi6LTiYM2KqQOj1N_D3CaYEq4QVi8i48MFudpWsaP2a2IiZMriTHbGSMsM6UiFhizkuLWyEtcbRbe=s0


Pertanyaan 2
Apakah perbedaan antara HTML dan XML?

Jawab:
Kita telah mengenal XML dari jawaban pertanyaan nomor 1. Sekarang saya akan membahas sekilas mengenai HTML.

HTML merupakan kepanjangan dari Hyptertext Markup Language, seperti XML, HTML juga merupakan sebuah bahasa mark up. HTML digunakan untuk membuat dan mendesain konten sebuah web. HTML memiliki banyak tag dan atribut yang dapat digunakan untuk mengatur tata letak dan struktur dari sebuat web. Dokumen HTML ditandai dengan ekstensi .htm atau .html.

Beberapa perbedaan utama dari HTML dan XML adalah:
1. XML berfokus kepada transfer data sedangkan fokus HTML adalah menampilkan atau mempresentasikan data.
2. XML didasarkan pada konten sedangkan HTML didasarkan pada format
3. XML sifatnya case sensitive sedangkan HTML case insensitive
4. Namespaces dapat digunakan pada XML tetapi tidak pada HTML
5. Kita harus menutup tag pada XML sedangkan di HTML tidak demikian
6. Tag XML bersifat extensible sedangkan tag HTML terbatas
7. HTML memiliki predefined tags sedangkan XML tidak memilikinya

Perbedaan HTML dan XML secara lebih mendetail dapat Anda lihat melalui link berikut
https://www.guru99.com/xml-vs-html-difference.html#:~:text=XML%20is%20abbreviation%20for%20eXtensible%20Markup%20Language%20whereas,is%20Case%20sensitive%20while%20HTML%20is%20Case%20insensitive.

Referensi tambahan
Perbedaan Antara JSON Dan XML (Protokol & format). (n.d.). Perbedaan antara objek dan istilah yang serupa.https://id.sawakinome.com/articles/protocols--formats/difference-between-json-and-xml-3.html

Berga, M. (2021, January 28). JSON vs XML: Which one is better? Blog | Imaginary Cloud.https://www.imaginarycloud.com/blog/json-vs-xml/

