import 'package:flutter/material.dart';

class FormScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return FormScreenState();
  }
}

class FormScreenState extends State<FormScreen> {
  String? _name;
  String? _sks;
  String? _day;
  String? _time;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  Widget _buildName() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Nama Mata Kuliah'),
      validator: (String? value) {
         if (value!.isEmpty) {
           setState(() =>
          _name="kosong");
          return 'Mohon masukkan nama mata kuliah';
        } 
        setState(() =>
          _name=value);
        return null;
      },
    );
  }

  Widget _buildSKS() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'SKS'),
      validator: (String? value) {
         if (value!.isEmpty) {
           setState(() =>
          _name="kosong");
          return 'Mohon masukkan sks mata kuliah';
        } 
        setState(() {
          _sks =value;
          return null;
        });
    
      }
    );
  }

  Widget _buildDay() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Hari'),
      validator: (String? value) {
        if (value!.isEmpty) {
          setState(() =>
          _name="kosong");
          return 'Mohon masukkan hari kuliah';
        } setState(() {
          _day =value;
        });
      },
    );
  }

  Widget _buildTime() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Jam Perkuliahan'),
      validator: (String? value) {
        if (value!.isEmpty) {
           setState(() =>
          _name="kosong");
          return 'Mohon masukkan jam kuliah';
        } setState(() {
          _time=value;
        });
      }
      );
  }

  Widget _submitButton() {
    return ElevatedButton(
      onPressed: () => {
        _formKey.currentState!.validate()
      },
      child: Text('Submit',
          style:
          TextStyle(color: Colors.white, fontSize: 16)));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("Schedule Kuliah")),
        body: Container(
            padding: EdgeInsets.only(top: 24),
            alignment: Alignment.center,
            margin: EdgeInsets.all(24),
            child: ListView(children: <Widget>[
              Center(child:Text(  
                  'Add Jadwal Matkul',  
                  style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),  
                )),
              Form(
                key: _formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    _buildName(),
                    _buildSKS(),
                    _buildDay(),
                    _buildTime(),
                    SizedBox(height: 100),
                    _submitButton(),
                  ],
                ))
              ,SizedBox(height: 5),
              Center(
                child: ElevatedButton(
                child: const Text('Lihat Jadwal', style: TextStyle(color: Colors.white, fontSize: 16)),
                onPressed: () {
                  showModalBottomSheet<void>(
                    context: context,
                    builder: (BuildContext context) {
                      return Container(
                        height: 500,
                        color: Colors.blueGrey.shade900,
                        child: Center(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              const Text ('Weekly Schedule', style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold)),
                                DataTable(  
                            columns: [  
                              DataColumn(label: Text(  
                                  'No',  
                                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)  
                              )),  
                              DataColumn(label: Text(  
                                  'Mata Kuliah',  
                                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)  
                              )),  
                              DataColumn(label: Text(  
                                  'SKS',  
                                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)  
                              )),
                               DataColumn(label: Text(  
                                  'Hari',  
                                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)  
                              )),  
                               DataColumn(label: Text(  
                                  'Jam',  
                                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)  
                              )),  
                            ],  
                            rows: [  
                              DataRow(cells: [  
                                DataCell(Text('1')),  
                                DataCell(Text('$_name')),  
                                DataCell(Text('$_sks')),  
                                DataCell(Text('$_day')),  
                                DataCell(Text('$_time')),  
                              ]),  
                              DataRow(cells: [  
                                DataCell(Text('2')),  
                                DataCell(Text('Alin')),  
                                DataCell(Text('3')),  
                                DataCell(Text('Rabu')),  
                                DataCell(Text('08.00 - 10.30')),  
                              ]),  
                              DataRow(cells: [  
                                DataCell(Text('3')),  
                                DataCell(Text('SDA')),  
                                DataCell(Text('4')),  
                                DataCell(Text('Selasa')),  
                                DataCell(Text('10.00 - 11.30')),
                              ]),  
                              DataRow(cells: [  
                                DataCell(Text('4')),  
                                DataCell(Text('SOSI')),  
                                DataCell(Text('3')),  
                                DataCell(Text('Jumat')),  
                                DataCell(Text('13.00 - 15.30')),
                              ])]),  
                              SizedBox(height: 10),
                              ElevatedButton(
                                child: const Text('Kembali'),
                                onPressed: () => Navigator.pop(context),
                              )
                            ]
                          )
                        )
                      );
                    }
                  );
                }
                )
              )

            ]
                
            )
        )
    );
  }
}
