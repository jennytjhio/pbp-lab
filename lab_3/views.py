from django.shortcuts import redirect, render
from lab_1.models import Friend
from lab_3.forms import FriendForm
from django.contrib.auth.decorators import login_required

def index(request):
    friends = Friend.objects.all()  # TODO Implement this
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url="/admin/login/")
def add_friend(request):
    if request.method == "POST":
        form = FriendForm(request.POST)
        if form.is_valid():
            form.save() 
            return redirect("lab_3:index")
        print(form.errors)
    form = FriendForm()
    context = {"form" : form}
    return render(request, "lab3_form.html", context)