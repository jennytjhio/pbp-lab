from django.http import request
from django.shortcuts import render, redirect
from lab_2.models import Note
from lab_4.forms import NoteForm
# Create your views here.

def index(request):
    notes = Note.objects.all() 
    response = {'notes': notes}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    if request.method == "POST":
        form = NoteForm(request.POST)
        if form.is_valid():
            form.save() 
            return redirect("lab_4:index")
    form = NoteForm()
    context = {"form" : form}
    return render(request, "lab4_form.html", context)

def note_list(request):
    notes = Note.objects.all() 
    response = {'notes': notes}
    return render(request, 'lab4_note_list.html', response)