from django.forms import ModelForm
from lab_2.models import Note
from django.forms.widgets import Input


class NoteForm(ModelForm):
    class Meta:
        model = Note
        fields = ["untuk", "dari", "judul", "pesan"]
        widgets = {
            "untuk": Input(attrs={"class": "form-control"}),
            "dari": Input(attrs={"class": "form-control"}),
            "judul": Input(attrs={"class": "form-control"}),
            "pesan": Input(attrs={"class": "form-control"})
        }
